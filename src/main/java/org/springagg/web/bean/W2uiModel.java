package org.springagg.web.bean;

/**
 * W2ui模型
 * <p>
 * w2ui 前端grid显示需要的字段
 * 
 * @author ArchX[archx@foxmail.com]
 */
public class W2uiModel {

    protected int recid;

    public int getRecid() {
        return recid;
    }

    public void setRecid(int recid) {
        this.recid = recid;
    }

}
